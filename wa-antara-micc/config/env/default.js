'use strict';

module.exports = {
  app: {
    title: 'Antara Insight - Media Insight',
    description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
    keywords: 'mongodb, express, angularjs, node.js, mongoose, passport',
    googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'GOOGLE_ANALYTICS_TRACKING_ID'
  },
  port: process.env.PORT || 3000,
  templateEngine: 'swig',
  sessionSecret: 'altegralabscommandcenter',
  sessionCollection: 'commandcenter',
  logo: 'modules/core/img/brand/logo.png',
  // favicon: 'modules/core/img/brand/favicon.ico',
  favicon: 'public/asset/favicon.ico',
  // local ip
  apiAddress: 'http://apps.antara-insight.id',
  elasticAddress: 'http://el.antara-insight.id/',
  ipElastic: 'el.antara-insight.id',
  // sessionAddress: ['139.59.245.121:11211'],
  sessionAddress: ['172.16.0.3:11211'],
  digitalOceanAPI: 'http://139.59.245.121:3035',
  formedAPI: 'http://al-api.antara-insight.id/api/v1.0.0',
  // formedAPI: 'http://localhost:3035/api/v1.0.0',
};
