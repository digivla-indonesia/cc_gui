'use strict';
angular.module('core.config', []);
angular.module('core.config').constant('coreConfig', {
    app: {
        baseUrl: 'http://localhost:3000/',
        onlinemonitoringAPI: 'api/onlinemonitoring/ridwankamil',
        altegraLabsAPI: 'http://139.59.245.121:5758/api/v1.0.0',
        authentication: ('altegralabs' + ':' + 'jSMOWnoPFgsHVpMvz5VrIt5kRbzGpI8u9EF1iFQyJQ=').toString('base64'),
        defaultMediaType: 'print',
        refreshInterval: 60000,
        //updateInterval: 1000 * 60 * 60 * 4, // Check update every 4 Hours
        updateInterval: 1000 * 10,
        limitsToDisplay: 10,
        limitsToHold: 100,
        // socketServer: 'http://localhost:8000/synchroscope#cc',
        socketServer: 'http://139.59.245.121:8000/synchroscope#cc',
       // socketServer: 'http://ccsocket.antara-insight.id/synchroscope#cc',
        socmedSocket: 'http://onlinemonitoring.id:3001',
        alertWatcher: 'http://localhost:3001'
    },
    client: {
        username: 'ridwankamil',
        logoHead: 'http://admin.antara-insight.id/asset/images/company/1475902371_logo esdm 72 x 72.png',
        emailReportRecipients: ['lesudiantoro@gmail.com', 'juwanda@gmail.com', 'muchtarfahmy14@gmail.com']
    },
    chart: {
        defaultOptions: {
            responsive: true,
            maintainAspectRatio: false,
            defaultFontColor: 'rgba(254,254,254,1)',
            defaultFontFamily: '"proxima_nova_thin", Georgia, sans-serif',
            title: { display: false },
            tooltips: {
                enabled: true,
                // mode: 'point',
                mode: 'label',
                xPadding: 10,
                yPadding: 10,
                backgroundColor: 'rgba(16,19,38,1)',
                titleFontColor: 'rgba(254,254,254,1)',
                titleFontSize: 18,
                bodyFontColor: 'rgba(254,254,254,1)',
                bodyFontSize: 14,
                callbacks: {
                    label: function(tooltipItem, data) {
                        var corporation = data.datasets[tooltipItem.datasetIndex].label;
                        var valor = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        var total = 0;

                        // console.log('tooltipItem.index: ' + tooltipItem.index);
                        // console.log('tooltipItem.datasetIndex: ' + tooltipItem.datasetIndex);
                        // console.log('valor: ' + valor);
                        // console.log('tooltipItem: ' + JSON.stringify(tooltipItem));

                        for (var i = 0; i < data.datasets.length; i++)
                            total += data.datasets[i].data[tooltipItem.index];
                        if (tooltipItem.datasetIndex != data.datasets.length - 1) {
                            // return corporation + " : $" + valor.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                            return corporation + ": " + valor;
                        } else {
                            // return [corporation + " : $" + valor.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'), "Total : $" + total];
                            return [corporation + ": " + valor, "Total: " + total]
                        }
                    }
                }
            },
            legend: {
                display: true,
                position: 'bottom',
                fullWidth: true,
                labels: {
                    fontColor: 'rgba(254,254,254,1)',
                    fontFamily: '"proxima_nova_thin", Georgia, sans-serif',
                    fontSize: 14
                }
            },
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: false,
                        fontColor: 'rgba(254,254,254,0.25)',
                        fontFamily: '"proxima_nova_thin", Georgia, sans-serif',
                        fontSize: 12
                    },
                    ticks: {
                        display: false,
                        fontColor: 'rgba(254,254,254,0.25)',
                        fontFamily: '"proxima_nova_thin", Georgia, sans-serif',
                        fontSize: 12
                    },
                    gridLines: {
                        drawBorder: false,
                        offsetGridLines: true,
                        color: 'rgba(254,254,254,0.1)'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: false,
                        fontColor: 'rgba(254,254,254,0.25)',
                        fontFamily: '"proxima_nova_thin", Georgia, sans-serif',
                        fontSize: 12
                    },
                    ticks: {
                        display: false,
                        fontColor: 'rgba(254,254,254,0.25)',
                        fontFamily: '"proxima_nova_thin", Georgia, sans-serif',
                        fontSize: 12
                    },
                    gridLines: {
                        drawBorder: false,
                        offsetGridLines: true,
                        color: 'rgba(254,254,254,0.1)'
                    }
                }]
            },
            elements: {
                point: {
                    backgroundColor: 'rgba(255,255,255,0)',
                    borderColor: 'rgba(255,255,255,0)'
                }
            }
        },
        datasetsDefaultOptions : {
            fill: false,
            lineTension: 0,
            borderWidth: 4,
            pointBackgroundColor: 'rgba(255,255,255,0)',
            pointBorderColor: 'rgba(255,255,255,0)'
        }
    },
    doughnut: {
        defaultOptions : {
            cutoutPercentage: 70
        },
        datasetsDefaultOptions : {
            borderWidth: 0
        }
    },
    colors: {
        netral: '#52c6d8',
        positive: '#2bb673',
        negative: '#ee028b',
        others: '#f2f2f2'
    },
    screen: {
        width: 1280,
        height: 720
    }
});
