'use strict';
angular.module('core.helper.update', []);
angular.module('core.helper.update').service('coreHelperUpdate', [ '$window', '$location', 'modelHTTP',
    function($window, $location, modelHTTP) {
        var service = {};
        service.checkLocal = function () {
            if ($window.localStorage.getItem('update') === 'available') {
                modelHTTP.runUpdate().then(function (response) {
                    if(response.data.status === 'running') {
                        $window.localStorage.setItem('update', 'running');
                        $location.path('/panels/update');
                    }
                });
            }
            else if ($window.localStorage.getItem('update') === 'running') {
                $location.path('/panels/update');
            }
            else if ($window.localStorage.getItem('update') === 'finish') {
                $window.localStorage.setItem('update', 'none');
                location.reload(true);
            }
            else {
                $window.localStorage.setItem('update', 'none');
            }
        };
        service.setLocal = function (response) {
            // Run update
            if(response.data.status === 'available') {
                $window.localStorage.setItem('update', 'available');
                modelHTTP.runUpdate().then(function (response) {
                    if(response.data.status === 'running') {
                        $window.localStorage.setItem('update', 'running');
                        $location.path('/panels/update');
                    }
                });
            }
            // Redirect page
            else if(response.data.status === 'running') {
                $window.localStorage.setItem('update', 'running');
                $location.path('/panels/update');
            }
            // No update
            else {
                $window.localStorage.setItem('update', 'none');
            }
        };
        return service;
    }
]);

