'use strict';
angular.module('model.mention', ['ngResource']);
angular.module('model.mention').factory('modelMention', ['Authentication', '$resource', '$http', '$interval', 'coreConfig', 'coreHelper',
    function(Authentication, $resource, $http, $interval, coreConfig, coreHelper) {
        var self = this;

        self.getPanelData = function (param, success) {
            var httpRes = $http.get(coreConfig.app.onlinemonitoringAPI).then(function(response) {
                var data = response.data.data['data_page'+param[0]];
                //console.log(data);
                switch (param[1]) {
                    case 'chart':
                        switch(param[0]) {
                            case 1:
                                success(data.graph);
                                break;
                            case 2:
                                success(data.graph_2);
                                break;
                            default:
                                success(data.graph);
                        }
                        break;
                    case 'doughnut':
                        var total = parseInt(data.netral) + parseInt(data.negatif) + parseInt(data.positif);
                        success([
                            {
                                labels: ["Positive", "Others"],
                                data: [
                                    (parseInt(data.positif) / total * 100).toFixed(1),
                                    ((total - parseInt(data.positif)) / total * 100).toFixed(1)
                                ]
                            },
                            {
                                labels: ["Neutral", "Others"],
                                data: [
                                    (parseInt(data.netral) / total * 100).toFixed(1),
                                    ((total - parseInt(data.positif)) / total * 100).toFixed(1)
                                ]
                            },
                            {
                                labels: ["Negative", "Others"],
                                data: [
                                    (parseInt(data.negatif) / total * 100).toFixed(1),
                                    ((total - parseInt(data.positif)) / total * 100).toFixed(1)
                                ]
                            }
                        ]);
                        break;
                    case 'progress':
                        var dataPercent = [];
                        var mapData = [];
                        var maxValue = 0;
                        for(var name in data.breakdown) {
                            var icon = '';
                            switch (name.toLowerCase()) {
                                case 'web':
                                    icon = 'globe';
                                    break;
                                case 'blog':
                                    icon = 'rss-square';
                                    break;
                                case 'twitter':
                                    icon = 'twitter-square';
                                    break;
                                case 'facebook':
                                    icon = 'facebook-square';
                                    break;
                                case 'instagram':
                                    icon = 'instagram';
                                    break;
                                case 'video':
                                    icon = 'youtube-play';
                                    break;
                                case 'news':
                                    icon = 'file-text-o';
                                    break;
                                case 'forum':
                                    icon = 'comments';
                                    break;
                                default:
                                    icon = 'rss-square';
                            }
                            mapData.push({
                                name: name.toLowerCase(),
                                icon: icon,
                                count: coreHelper.toShortCurrency(parseInt(data.breakdown[name]), 1),
                                percent: parseInt(data.breakdown[name]),
                                count_original: parseInt(data.breakdown[name]),
                                name_original: name
                            });
                            if (parseInt(data.breakdown[name]) > maxValue) {
                                maxValue = parseInt(data.breakdown[name]);
                            }
                        }
                        for(var i in mapData) {
                            var tempDataPercent = mapData[i];
                            tempDataPercent.percent = tempDataPercent.percent / maxValue * 100;
                            dataPercent.push(tempDataPercent);
                        }
                        dataPercent.sort(function(a, b) {
                            return (b.percent - a.percent);
                        });
                        success(dataPercent);
                        // console.log(JSON.stringify(dataPercent));
                        break;
                    case 'addprogress':
                        var dataPercent = [];
                        var mapData = [];
                        var maxValue = 0;
                        var kb = data.keyword_breakdown[param[2]];

                        for(var i=0;i<param[3].length;i++){
                            var icon = '';
                            switch (param[3][i].name.toLowerCase()) {
                                case 'web':
                                    icon = 'globe';
                                    break;
                                case 'blog':
                                    icon = 'rss-square';
                                    break;
                                case 'twitter':
                                    icon = 'twitter-square';
                                    break;
                                case 'facebook':
                                    icon = 'facebook-square';
                                    break;
                                case 'instagram':
                                    icon = 'instagram';
                                    break;
                                case 'video':
                                    icon = 'youtube-play';
                                    break;
                                case 'news':
                                    icon = 'file-text-o';
                                    break;
                                case 'forum':
                                    icon = 'comments';
                                    break;
                                default:
                                    icon = 'rss-square';
                            }
                            mapData.push({
                                name: param[3][i].name.toLowerCase(),
                                icon: icon,
                                count: coreHelper.toShortCurrency(parseInt(param[3][i].count_original) + parseInt(kb.breakdown[param[3][i].name_original]), 1),
                                percent: parseInt(data.breakdown[name]),
                                count_original: parseInt(param[3][i].count_original) + parseInt(kb.breakdown[param[3][i].name_original]),
                                name_original: param[3][i].name_original
                            });
                            if (parseInt(data.breakdown[name]) > maxValue) {
                                maxValue = parseInt(data.breakdown[name]);
                            }
                        }
                        for(var i in mapData) {
                            var tempDataPercent = mapData[i];
                            tempDataPercent.percent = tempDataPercent.percent / maxValue * 100;
                            dataPercent.push(tempDataPercent);
                        }
                        dataPercent.sort(function(a, b) {
                            return (b.percent - a.percent);
                        });
                        success(dataPercent);
                        console.log(JSON.stringify(dataPercent));
                        break;
                    case 'subsprogress':
                        var dataPercent = [];
                        var mapData = [];
                        var maxValue = 0;
                        var kb = data.keyword_breakdown[param[2]];

                        for(var i=0;i<param[3].length;i++){
                            var icon = '';
                            switch (param[3][i].name.toLowerCase()) {
                                case 'web':
                                    icon = 'globe';
                                    break;
                                case 'blog':
                                    icon = 'rss-square';
                                    break;
                                case 'twitter':
                                    icon = 'twitter-square';
                                    break;
                                case 'facebook':
                                    icon = 'facebook-square';
                                    break;
                                case 'instagram':
                                    icon = 'instagram';
                                    break;
                                case 'video':
                                    icon = 'youtube-play';
                                    break;
                                case 'news':
                                    icon = 'file-text-o';
                                    break;
                                case 'forum':
                                    icon = 'comments';
                                    break;
                                default:
                                    icon = 'rss-square';
                            }
                            mapData.push({
                                name: param[3][i].name.toLowerCase(),
                                icon: icon,
                                count: coreHelper.toShortCurrency(parseInt(param[3][i].count_original) - parseInt(kb.breakdown[param[3][i].name_original]), 1),
                                percent: parseInt(data.breakdown[name]),
                                count_original: parseInt(param[3][i].count_original) - parseInt(kb.breakdown[param[3][i].name_original]),
                                name_original: param[3][i].name_original
                            });
                            if (parseInt(data.breakdown[name]) > maxValue) {
                                maxValue = parseInt(data.breakdown[name]);
                            }
                        }
                        for(var i in mapData) {
                            var tempDataPercent = mapData[i];
                            tempDataPercent.percent = tempDataPercent.percent / maxValue * 100;
                            dataPercent.push(tempDataPercent);
                        }
                        dataPercent.sort(function(a, b) {
                            return (b.percent - a.percent);
                        });
                        success(dataPercent);
                        console.log(JSON.stringify(dataPercent));
                        break;
                    case 'progress_2':
                        var totalArr = {};
                        var mapData = {};
                        var limit = 5; // Limit progress data up to 5 items
                        // Map data
                        for (var name in data.top_issue) {
                            var limitCount = 0;
                            mapData[name] = {};
                            for(var i in data.top_issue[name]) {
                                if (limitCount < limit) {
                                    if (!totalArr[i]) {
                                        totalArr[i] = 0;
                                    }
                                    mapData[name][i] = {
                                        name: data.top_issue[name][i].issue,
                                        count: parseInt(data.top_issue[name][i].post),
                                        percent: parseInt(data.top_issue[name][i].post)
                                    };
                                    totalArr[i] += parseInt(data.top_issue[name][i].post);
                                }
                                limitCount++;
                            }
                        }
                        // Add percent
                        var dataPercent = {};
                        for (var name in mapData) {
                            var limitCount = 0;
                            dataPercent[name] = {};
                            for(var i in mapData[name]) {
                                if (limitCount < limit) {
                                    var tempDataPercent = mapData[name][i];
                                    tempDataPercent.percent = tempDataPercent.percent / totalArr[i] * 100;
                                    tempDataPercent.count = coreHelper.toShortCurrency(parseInt(tempDataPercent.count), 1);
                                    dataPercent[name][i] = tempDataPercent;
                                }
                                limitCount++;
                            }
                            // Add doughnut data from positif
                            if (name === 'positif') {
                                var limitCount = 0;
                                dataPercent['doughnut'] = { data: [], labels: [] };
                                for(var i in mapData['positif']) {
                                    if (limitCount < limit) {
                                        dataPercent['doughnut'].data.push(mapData['positif'][i]['count']);
                                        dataPercent['doughnut'].labels.push(mapData['positif'][i]['name']);
                                    }
                                    limitCount++;
                                }
                            }
                        }
                        success(dataPercent);
                        //console.log(totalArr);
                        break;
                    case 'count':
                        success({
                            tm: coreHelper.toShortCurrency(parseInt(data.total_mentions)),
                            tr: coreHelper.toShortCurrency(parseInt(data.reach_day)),
                            md: coreHelper.toShortCurrency(parseInt(data.mention_day)),
                            mh: coreHelper.toShortCurrency(parseInt(data.mention_hour)),
                            all_totals: {
                                tm: parseInt(data.total_mentions),
                                tr: parseInt(data.reach_day),
                                md: parseInt(data.mention_day),
                                mh: parseInt(data.mention_hour),
                            }
                        });
                        break;
                    case 'subscount':
                        var kb = data.keyword_breakdown[param[2]];

                        success({
                            tm: coreHelper.toShortCurrency(parseInt(param[3].tm) - parseInt(kb.total_mentions)),
                            tr: coreHelper.toShortCurrency(parseInt(param[3].tr) - parseInt(kb.reach_day)),
                            md: coreHelper.toShortCurrency(parseInt(param[3].md) - parseInt(kb.mention_day)),
                            mh: coreHelper.toShortCurrency(parseInt(param[3].mh) - parseInt(kb.mention_hour)),
                            all_totals: {
                                tm: parseInt(parseInt(param[3].tm) - parseInt(kb.total_mentions)),
                                tr: parseInt(parseInt(param[3].tr) - parseInt(kb.reach_day)),
                                md: parseInt(parseInt(param[3].md) - parseInt(kb.mention_day)),
                                mh: parseInt(parseInt(param[3].mh) - parseInt(kb.mention_hour)),
                            }
                        });
                        break;
                    case 'addcount':
                        var kb = data.keyword_breakdown[param[2]];

                        success({
                            tm: coreHelper.toShortCurrency(parseInt(param[3].tm) + parseInt(kb.total_mentions)),
                            tr: coreHelper.toShortCurrency(parseInt(param[3].tr) + parseInt(kb.reach_day)),
                            md: coreHelper.toShortCurrency(parseInt(param[3].md) + parseInt(kb.mention_day)),
                            mh: coreHelper.toShortCurrency(parseInt(param[3].mh) + parseInt(kb.mention_hour)),
                            all_totals: {
                                tm: parseInt(parseInt(param[3].tm) + parseInt(kb.total_mentions)),
                                tr: parseInt(parseInt(param[3].tr) + parseInt(kb.reach_day)),
                                md: parseInt(parseInt(param[3].md) + parseInt(kb.mention_day)),
                                mh: parseInt(parseInt(param[3].mh) + parseInt(kb.mention_hour)),
                            }
                        });
                        break;
                    case 'topposts':
                        var resArr = [];
                        for(var i=1;i<=10;i++){
                            resArr.push(data.top_posts[i.toString()]);
                        }
                        success(resArr);

                        break;
                    default:
                        success(data);
                }
            });
            return httpRes;
        };

        self.getWords = function (success) {
            Authentication.getClientAssets(function(result){
                return $http.get('api/wordcloud/'+result.name).then(function(response) {
                    success(response.data.data.data);
                });
            });
        };

        return self;
    }
]);
