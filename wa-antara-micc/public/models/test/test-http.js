'use strict';
angular
    .module('model.test')
    .service('modelHTTP', ChartData);

ChartData.$inject = ['$resource', '$http', '$rootScope', 'coreConfig'];
function ChartData($resource, $http, $rootScope, coreConfig) {

    this.HTTPPost = function(url, datas){
        return $http.post('/api/digivla-requester', datas);
    };

    this.emailer = function(datas){
        return $http.post('/api/email-sender', datas);
    };

    this.sendReport = function(report){
        return $http.post('/api/email-report', {
            subject: 'Overview Report for ' + new Date(Date.now()).toString(),
            data: report,
            to: coreConfig.client.emailReportRecipients.join(', ')
        });
    };

    this.checkUpdate = function(){
        return $http.post('/api/update', { action: 'check' });
    };

    this.runUpdate = function(){
        return $http.post('/api/update', { action: 'run' });
    };

    this.HTTPGet = function(url, datas){
        // var url = source +
        // 'gc=' + getFilter().gc +
        // '&ms=' + getFilter().ms +
        // '&sc=' + getFilter().sc +
        // '&mi=' + getFilter().mi +
        // '&tf=' + getFilter().tf +
        // '&df=' + getFilter().from +
        // '&dt=' + getFilter().to +
        // '&code=' + code;

        // return $http.get(url);
        return $http.get(url);
    };

    this.getGraph = function(source, code){
        var url = source +
        'gc=' + getFilter().gc +
        '&ms=' + getFilter().ms +
        '&sc=' + getFilter().sc +
        '&mi=' + getFilter().mi +
        '&tf=' + getFilter().tf +
        '&df=' + getFilter().from +
        '&dt=' + getFilter().to +
        '&code=' + code;

        return $http.get(url);
    };

    this.EWSChartData = function(source, code){
        var mskapiURL = "/api/dashboards/oldews";
        var categoryArr = [], mediaArr = [];
        var from = ($rootScope.dateFrom !== '') ? new Date($rootScope.dateFrom) : '',
            to = ($rootScope.dateTo !== '') ? new Date($rootScope.dateTo) : '';

        var sentimentLabels = ["0", "Potential", "Emerging", "Current", "Crisis"];

        if (from !== '' && to !== '') {
            from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
            to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
        }


        // get categories
        if($rootScope.selectedSubCategory.category_id === 'All Category'){
            for(var i=0;i<$rootScope.allCategories.length;i++){
                if(i > 0) categoryArr.push($rootScope.allCategories[i].category_id);
            }
        } else{
            categoryArr = [$rootScope.selectedSubCategory.category_id];
        }

        // get medias
        if($rootScope.selectedMediaSelection.user_media_id === '00'){
            for(var i=0;i<$rootScope.allMedias.length;i++){
                if(i > 0) mediaArr.push($rootScope.allMedias[i].user_media_id);
            }
        } else{
            mediaArr = [$rootScope.selectedMediaSelection.user_media_id];
        }

        var assets = {
            'category_id': categoryArr,
            'media_id': mediaArr,
            'best': 20,
            'time_frame': $rootScope.selectedPeriodOptions.value,
            'date_from': from,
            'date_to': to
        };

        var abs = {
            'a': '054B',
            // 'keys': $scope.authentication.user.user_detail.usr_keys,
            // 'uid': $scope.authentication.user.user_detail.usr_uid,
            // 'client_id': $scope.authentication.user.user_detail.usr_client_id,
            'data': btoa(JSON.stringify(assets))
        };

        return $http({
            url: mskapiURL,
            method: "POST",
            data: abs
        });
    };

    return this;
}
