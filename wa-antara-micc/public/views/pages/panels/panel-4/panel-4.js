'use strict';
angular.module('view.page.panels.panel-4', []);
angular.module('view.page.panels.panel-4').component('pPanel4', {
    templateUrl: 'views/pages/panels/panel-4/panel-4.html',
    controller: [
        function() {
            var $ctrl = this;
            $ctrl.onInit = function () {
                $ctrl.addClass('outer-element')
            };
        }
    ]
});
