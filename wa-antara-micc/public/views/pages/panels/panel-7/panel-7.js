'use strict';
angular.module('view.page.panels.panel-7', []);
angular.module('view.page.panels.panel-7').component('pPanel7', {
    templateUrl: 'views/pages/panels/panel-7/panel-7.html',
    controller: [
        'Authentication', '$window', '$scope', '$element', '$interval', '$timeout', 'coreHelper', 'coreHelperChart', 'modelMention', 'coreConfig',
        'modelHTTP', '$ync', 'modelSocketSocmed', 'modelSocketAlertWatcher',
        function(
            Authentication, $window, $scope, $element, $interval, $timeout, coreHelper, coreHelperChart, modelMention, coreConfig,
            modelHTTP, $ync, modelSocketSocmed, modelSocketAlertWatcher
        ) {
            var $ctrl = this;
            var $intervalPromise = null;
            var $reportTimer = null;
            var $timeoutPromise = null;
            $ctrl.periodSelection = {};
            $ctrl.model = {
                mention: {}
            };
            $ctrl.systemalert = {
                state: {
                    socialMention: false,
                    keyInfluencer: false,
                    formalMedia: false
                },
                interval: 3000
            };
            $ctrl.gaugejs = [];

            $ctrl.$onInit = function () {

                // get category
                var categoryDatas = {
                    "url": "/client/getassets",
                    "method": "POST",
                    "data": {}
                };

                modelHTTP.HTTPPost('/api/digivla-requester', categoryDatas).success(function(response){
                    // category
                    $ctrl.categorySet = response.data.data.data;
                    $ctrl.categorySelection = $ctrl.categorySet[0].category_set;
                    $scope.categorySelection = $ctrl.categorySelection;

                    // period
                    $ctrl.periodSelection.tf = 7;
                    $ctrl.periodSelection.df = '';
                    $ctrl.periodSelection.dt = '';
                    $scope.periodSelection = $ctrl.periodSelection;

                    // mediaset
                    $ctrl.mediaTypeSelection = coreConfig.app.defaultMediaType;
                    $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;

                    changeFilter();


                    // connecting to socmed socket
                    modelSocketSocmed.on("alert-cc", function(alerts){
                        // console.log('alerts: ' + JSON.stringify(alerts));
                        $ctrl.startAlert('socialmention');
                    });
                    modelSocketSocmed.on("alert-cc-m", function(alerts){
                        // console.log('alerts: ' + JSON.stringify(alerts));
                        $ctrl.startAlert('keyinfluencer');
                    });

                    $timeout(function(){
                        $scope.$watch(function(){
                            return $ctrl.categorySelection;
                        }, function(){
                            $scope.categorySelection = $ctrl.categorySelection;
                            justRefresh();
                        });

                        $scope.$watch(function(){
                            return $ctrl.mediaTypeSelection;
                        }, function(){
                            $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                            justRefresh();
                        });

                        $scope.$watch(function(){
                            return $ctrl.periodSelection;
                        }, function(){
                            $scope.periodSelection = $ctrl.periodSelection;
                            justRefresh();
                        });


                        // between browsers
                        $scope.$watch('categorySelection', function(newValues){
                            if ($ctrl.categorySelection !== newValues) {
                                $ctrl.categorySelection = newValues;
                                $scope.categorySelection = $ctrl.categorySelection;
                                justRefresh();
                            }
                        });

                        $scope.$watch('mediaTypeSelection', function(newValues){
                            if ($ctrl.mediaTypeSelection !== newValues) {
                                $ctrl.mediaTypeSelection = newValues;
                                $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                                justRefresh();
                            }
                        });

                        $scope.$watch('periodSelection', function(newValues){
                            if ($ctrl.periodSelection !== newValues) {
                                $ctrl.periodSelection = newValues;
                                $scope.periodSelection = $ctrl.periodSelection;
                                justRefresh();
                            }
                        });
                    }, 10000);


                    // auto refresh
                    $interval(function(){
                        // $ctrl.model.mention.chart = {};
                        // changeFilter();
                        justRefresh();
                    }, coreConfig.app.refreshInterval);

                });

                // $ctrl.startAlert();
                initMentionGauge();
                angular.element($window).bind('resize', onWindowResize);
            };
            $ctrl.$onDestroy = function () {
                $timeout.cancel($timeoutPromise);
                $interval.cancel($intervalPromise);
            };
            $ctrl.startAlert = function (type) {
                $ctrl.stopAlert();
                $intervalPromise = $interval(function() {
                    $ctrl.systemalert.state.socialMention = false;
                    $ctrl.systemalert.state.keyInfluencer = false;
                    $ctrl.systemalert.state.formalMedia = false;
                    
                    $timeoutPromise = $timeout(function() {
                        //console.log('timeout');
                        switch(type){
                            case 'socialmention':
                                $ctrl.systemalert.state.socialMention = true;
                                break;
                            case 'keyinfluencer':
                                $ctrl.systemalert.state.keyInfluencer = true;
                                break;
                            case 'formalmedia':
                                $ctrl.systemalert.state.formalMedia = true;
                                break;
                            default:
                                $ctrl.systemalert.state.socialMention = true;
                                $ctrl.systemalert.state.keyInfluencer = true;
                                $ctrl.systemalert.state.formalMedia = true;
                        }

                    }, 0);
                }, $ctrl.systemalert.interval);
            };
            $ctrl.stopAlert = function (type) {
                if ($intervalPromise) {
                    $interval.cancel($intervalPromise);
                    $intervalPromise = null;
                }
                
                switch(type){
                    case 'socialmention':
                        $ctrl.systemalert.state.socialMention = false;
                        break;
                    case 'keyinfluencer':
                        $ctrl.systemalert.state.keyInfluencer = false;
                        break;
                    case 'formalmedia':
                        $ctrl.systemalert.state.formalMedia = false;
                        break;
                    default:
                        $ctrl.systemalert.state.socialMention = false;
                        $ctrl.systemalert.state.keyInfluencer = false;
                        $ctrl.systemalert.state.formalMedia = false;
                }
            };
            $ctrl.toggleAlert = function () {
                if ($intervalPromise) {
                    $ctrl.stopAlert('');
                }
                else {
                    $ctrl.startAlert('');
                }
            };
            $ctrl.generateReport = function () {
                var timer = 0;
                if (!$window.localStorage.getItem('report')) {
                    alert('Generating report. This may take a few minutes. Please wait..');
                    timer = coreConfig.app.refreshInterval;
                }
                coreHelper.initReport();
                var report = JSON.parse($window.localStorage.getItem('report'));
                report.client = coreConfig.client.username;
                $window.localStorage.setItem('report', JSON.stringify(report));

                //console.log(report);

                // Delay request until all panel store their report
                $reportTimer = $timeout(function () {
                    modelHTTP.sendReport(report).then(onSendReport, onSendReportError);
                }, timer);
            };
            function onSendReport(response) {
                alert(response.data);
            }
            function onSendReportError(response) {
                console.log(response);
                alert('Failed to generate report. Please try again later.');
            }
            function onMentionChart(chart) {
                var defaultOptions  = angular.copy(coreConfig.chart.defaultOptions);
                $ctrl.model.mention.chart.options = $.extend(true,  defaultOptions, {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: true,
                                fontColor: 'rgba(254,254,254,1)'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                display: true,
                                fontColor: 'rgba(254,254,254,1)'
                            }
                        }]
                    }
                });
                // Add configurations for each line
                $ctrl.model.mention.chart.datasets = coreHelperChart.createDatasets(chart.data);
                // Fix chart font size
                $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);
            }
            function onMentionProgress(progress) {
                $ctrl.model.mention.progress = progress[0];
                // console.log($ctrl.model.mention);
            }

            function initMentionGauge(){
                var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
                var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
                var pointerColors = ['#ffef00', '#ee8200', '#e95353', '#e62c34'];
                var flag, issuesArr = ["potential", "emerging", "current", "crisis"];
                var opts = {
                    angle: -0.23,
                    lineWidth: 0.1,
                    radiusScale: 0.8,
                    pointer: {
                        length: 0.3,
                        strokeWidth: 0.05,
                        color: '#000000'
                    },
                    staticZones: [
                        {strokeStyle: "#016fc3", min: 100/6 * 0, max: 100/6 * 1},
                        {strokeStyle: "#ffef00", min: 100/6 * 1, max: 100/6 * 2},
                        {strokeStyle: "#ee8200", min: 100/6 * 2, max: 100/6 * 3},
                        {strokeStyle: "#ee8200", min: 100/6 * 3, max: 100/6 * 4},
                        {strokeStyle: "#e95353", min: 100/6 * 4, max: 100/6 * 5},
                        {strokeStyle: "#e40013", min: 100/6 * 5, max: 100/6 * 6}
                    ],
                    renderTicks: {
                        divisions: 6,
                        divWidth: 5,
                        divLength: 1,
                        divColor: '#1c1737'
                    },
                    limitMax: false,
                    limitMin: false,
                    colorStart: 'transparent',
                    colorStop: 'transparent',
                    strokeColor: '#E0E0E0',
                    generateGradient: true,
                    highDpiSupport: true
                };

                for(var i=0;i<4;i++){
                    flag = issuesArr[i];

                    var canvas = $element.find(".c-gauge")[i];
                    canvas.width = Math.round(100/1280 * w);
                    canvas.height = Math.round(100/1280 * w);
                    opts.pointer.color = pointerColors[i];
                    $ctrl.gaugejs.push(new Gauge(canvas).setOptions(opts));
                    // $ctrl.gaugejs[i].maxValue = gauges.gaugeConfig.total;
                    $ctrl.gaugejs[i].maxValue = 100;
                    $ctrl.gaugejs[i].setMinValue(0);
                    $ctrl.gaugejs[i].animationSpeed = 32;
                    // $ctrl.gaugejs[i].set(gauges.gaugeConfig[flag]);
                    $ctrl.gaugejs[i].set(0);
                }
            }

            function onMentionGauge(gauges) {
                //console.log(gauges);
                var flag, issuesArr = ["potential", "emerging", "current", "crisis"];

                for(var i=0;i<4;i++){
                    flag = issuesArr[i];

                    var canvas = $element.find(".c-gauge")[i];
                    //$ctrl.gaugejs[i].maxValue = gauges.gaugeConfig.total;
                    $ctrl.gaugejs[i].set(gauges.gaugeConfig[flag]/gauges.gaugeConfig.total * 100);
                }

            }
            function onWindowResize() {
                // Fix chart font size
                if ($ctrl.model.mention.chart.options) {
                    $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);
                    $scope.$apply();
                }
            }

            function justRefresh(){
                var datas = {
                    "url": "/formed/getews",
                    "method": "POST",
                    "data": {
                        "gc": $ctrl.categorySelection,
                        "sc": "All Category",
                        "tf": $ctrl.periodSelection.tf,
                        "df": $ctrl.periodSelection.df,
                        "dt": $ctrl.periodSelection.dt,
                        "media_type": $ctrl.mediaTypeSelection
                    }
                };

                modelHTTP.HTTPPost('/api/digivla-requester', datas).success(function(response){
                    $ctrl.model.mention.chart = response.data.data;
                    onMentionChart($ctrl.model.mention.chart);
                    onMentionGauge($ctrl.model.mention.chart);
                    $ctrl.stopAlert('formalmedia');
                    // send alert
                    checkAlert(response.data.data.alertProperties);
                    
                });
            }

            function changeFilter(){
                var datas = {
                    "url": "/formed/getews",
                    "method": "POST",
                    "data": {
                        "gc": $ctrl.categorySelection,
                        "sc": "All Category",
                        "tf": $ctrl.periodSelection.tf,
                        "df": $ctrl.periodSelection.df,
                        "dt": $ctrl.periodSelection.dt,
                        "media_type": $ctrl.mediaTypeSelection
                    }
                };

                modelHTTP.HTTPPost('/api/digivla-requester', datas).success(function(response){
                    $ctrl.stopAlert('');
                    $ctrl.lastSendEmail = '';
                    $ctrl.model.mention.chart = response.data.data;
                    onMentionChart($ctrl.model.mention.chart);
                    onMentionGauge($ctrl.model.mention.chart);

                    // send alert
                    checkAlert(response.data.data.alertProperties);
                });
            }

            function checkAlert(datas){
                if(datas.length > 0){
                    if($ctrl.lastSendEmail !== ''){
                        $ctrl.startAlert('formalmedia');
                        if(Math.round( (new Date() - $ctrl.lastSendEmail) / (60*60*1000) ) > 1){
                            modelHTTP.emailer({client_id: coreConfig.client.username, datas: datas, recipients: coreConfig.client.emailReportRecipients.join(', ')}).success(function(response){
                                $ctrl.lastSendEmail = new Date();
                            });
                        }

                    } else{
                        modelHTTP.emailer({client_id: coreConfig.client.username, datas: datas, recipients: coreConfig.client.emailReportRecipients.join(', ')}).success(function(response){
                            $ctrl.lastSendEmail = new Date();
                            $ctrl.startAlert('formalmedia');
                        });
                    }

                }

                return;
            }

            Authentication.getClientAssets(function(result){
                $YNC.latencyTest = 100
                $ync($scope, ['categorySelection', 'periodSelection', 'mediaTypeSelection'], result.clientSocket);
            });
        }
    ]
});
