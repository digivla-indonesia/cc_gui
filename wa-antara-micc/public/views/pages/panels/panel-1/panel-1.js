'use strict';
angular.module('view.page.panels.panel-1', []);
angular.module('view.page.panels.panel-1').component('pPanel1', {
    templateUrl: 'views/pages/panels/panel-1/panel-1.html',
    controller: ['$interval', '$window', '$scope', '$element', 'coreHelper', 'coreHelperChart', 'modelMention', 'modelTest', 'coreConfig',
        function($interval, $window, $scope, $element, coreHelper, coreHelperChart, modelMention, modelTest, coreConfig) {
            var $ctrl = this;
            $ctrl.interval = [];
            $ctrl.model = {
                mention: {}
            };

            $ctrl.$onInit = function () {
                // Get mention data
                $ctrl.model.mention.chart = {};
                $ctrl.model.mention.progress = {};

                modelMention.getPanelData([1, 'chart'], onMentionChart);
                modelMention.getPanelData([1, 'progress'], onMentionProgress);
                modelMention.getPanelData([1, 'count'], onMentionCount);

                angular.element($window).bind('resize', onWindowResize);

                // auto refresh
                $ctrl.interval[3] = $interval(function(){
                    $ctrl.model.mention.chart = {};
                    $ctrl.model.mention.progress = {};
                    
                    modelMention.getPanelData([1, 'chart'], onMentionChart);
                    modelMention.getPanelData([1, 'progress'], onMentionProgress);
                    modelMention.getPanelData([1, 'count'], onMentionCount);
                }, coreConfig.app.refreshInterval);
            };
            $ctrl.$onDestroy = function () {
                $interval.cancel($ctrl.interval[0]);
                $interval.cancel($ctrl.interval[1]);
                $interval.cancel($ctrl.interval[2]);
                $interval.cancel($ctrl.interval[3]);
            };

            $ctrl.chartUpdate = function () {
                $ctrl.interval[0] = $interval(function () {
                    modelMention.getPanelData([1, 'chart'], function(chart) {
                        $ctrl.model.mention.chart = Object.assign({}, $ctrl.model.mention.chart, chart);
                    });
                }, coreConfig.app.refreshInterval);
            };
            $ctrl.progressUpdate = function () {
                $ctrl.interval[1] = $interval(function () {
                    modelMention.getPanelData([1, 'progress'], function (progress) {
                        $ctrl.model.mention.progress = Object.assign({}, $ctrl.model.mention.progress, progress);

                        $ctrl.storeMB($ctrl.model.mention.progress);
                    });
                }, coreConfig.app.refreshInterval);
            };
            $ctrl.countUpdate = function () {
                $ctrl.interval[2] = $interval(function () {
                    modelMention.getPanelData([1, 'count'], function (count) {
                        $ctrl.model.mention.count = Object.assign({}, $ctrl.model.mention.count, count);

                        $ctrl.storeTM($ctrl.model.mention.count.tm);
                    });
                }, coreConfig.app.refreshInterval);
            };

            $ctrl.storeTM = function (tm) {
                coreHelper.initReport();
                var localReport = JSON.parse($window.localStorage.getItem('report'));
                localReport.stats.tm = tm;
                //console.log(localReport.stats.tm);
                $window.localStorage.setItem('report', JSON.stringify(localReport));
            };

            $ctrl.storeMB = function (mb) {
                coreHelper.initReport();
                var localReport = JSON.parse($window.localStorage.getItem('report'));
                localReport.stats.mb = mb;
                //console.log(localReport.stats.mb);
                $window.localStorage.setItem('report', JSON.stringify(localReport));
            };

            function onMentionChart(chart) {
                // Add chart data
                $ctrl.model.mention.chart = chart;

                // adding legend click event
                var original = Chart.defaults.global.legend.onClick;
                coreConfig.chart.defaultOptions.legend.onClick = function(e, legendItem) {
                    /* do custom stuff here */
                    var index = legendItem.datasetIndex;
                    var ci = this.chart;
                    var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;

                    if(ci.getDatasetMeta(index).hidden){
                        // alert("tambahin");
                        modifyTotal(true, legendItem.text);
                        modifyProgress(true, legendItem.text);
                    } else{
                        // alert("kurangin");
                        modifyTotal(false, legendItem.text);
                        modifyProgress(false, legendItem.text);
                    }

                    original.call(this, e, legendItem);
                };
                
                // Add global configuration
                $ctrl.model.mention.chart.options = angular.copy(coreConfig.chart.defaultOptions);
                // Add configurations for each line
                $ctrl.model.mention.chart.datasets = coreHelperChart.createDatasets(chart.data);
                // Fix chart font size
                $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);

                $ctrl.chartUpdate();
            }
            function onMentionProgress(progress) {
                $ctrl.model.mention.progress = progress;

                $ctrl.progressUpdate();
            }
            function onMentionCount(count) {
                $ctrl.model.mention.count = count;

                $ctrl.countUpdate();
            }
            function onWindowResize() {
                // Fix chart font size
                if ($ctrl.model.mention.chart.options) {
                    $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);
                    $scope.$apply();
                }
            }

            // substract all total
            function modifyTotal(add, keyword){
                var modTotal = (add) ? 'addcount' : 'subscount';
                modelMention.getPanelData([1, modTotal, keyword, $ctrl.model.mention.count.all_totals], function (count) {
                    $ctrl.model.mention.count = Object.assign({}, $ctrl.model.mention.count, count);

                    $ctrl.storeTM($ctrl.model.mention.count.tm);
                });
            }

            function modifyProgress(add, keyword){
                var modProgress = (add) ? 'addprogress' : 'subsprogress';
                modelMention.getPanelData([1, modProgress, keyword, $ctrl.model.mention.progress], function (progress) {
                    $ctrl.model.mention.progress = Object.assign({}, $ctrl.model.mention.progress, progress);

                    $ctrl.storeMB($ctrl.model.mention.progress);
                });
            }

        }
    ]
});
