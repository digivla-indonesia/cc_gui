'use strict';
angular.module('view.component.footer', []);
angular.module('view.component.footer').component('cFooter', {
    templateUrl: 'views/components/footers/footer.html',
    controller: [ 'coreConfig', 'Authentication',
        function (coreConfig, Authentication) {
            var $ctrl = this;
            $ctrl.logoHead = coreConfig.logoHead;
            Authentication.getClientAssets(function(result){
            	// return result.clientLogo;
            	$ctrl.clientLogo = result.clientLogo
            });
        }
    ]
});
