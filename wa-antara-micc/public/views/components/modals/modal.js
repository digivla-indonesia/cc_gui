'use strict';
angular.module('view.component.modal', []);
angular.module('view.component.modal').component('cModal', {
    templateUrl: 'views/components/modals/modal.html',
    bindings: {
        modalInstance: '<',
        resolve: '<'
    },
    controller: [
        function() {
            var date_from = new Date();
            var $ctrl = this;

            date_from.setDate(date_from.getDate() - 6);

            $ctrl.prop = {
                config: {
                    title: 'Untitled'
                },
                datepicker: {
                    fromDate: {
                        // date: new Date(2017, 12, 1),
                        date: date_from,
                        isOpen: false
                    },
                    toDate: {
                        // date: new Date(2017, 12, 10),
                        date: new Date(),
                        isOpen: false
                    }
                }
            };
            $ctrl.data = {};
            $ctrl.$onInit = function() {
                $ctrl.prop.config = $ctrl.resolve.modalConfig;
                $ctrl.data = $ctrl.resolve.modalData;
            };
            $ctrl.close = function() {
                // $ctrl.modalInstance.close($ctrl.data);
                $ctrl.modalInstance.close({from: $ctrl.prop.datepicker.fromDate.date, to: $ctrl.prop.datepicker.toDate.date});
            };
            $ctrl.dismiss = function($event) {
                $event.preventDefault();
                $ctrl.modalInstance.dismiss('cancel');
            };
        }
    ]
});
