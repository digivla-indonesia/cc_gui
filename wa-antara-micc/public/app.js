'use strict';
angular.module('micc', ['ngAnimate', 'ngRoute', 'ui.bootstrap', 'chart.js', 'core', 'view', 'model']);

// Generals
angular.module('micc').config(['$locationProvider',
    function($locationProvider) {
        $locationProvider.hashPrefix('!');
    }
]);

// Chart.js
angular.module('micc').config(['ChartJsProvider', 'coreConfig',
    function(ChartJsProvider, coreConfig) {
        ChartJsProvider.setOptions('line', Object.assign({}, coreConfig.chart.defaultOptions));
        ChartJsProvider.setOptions('bar', Object.assign({}, coreConfig.chart.defaultOptions));
        ChartJsProvider.setOptions('doughnut', coreConfig.doughnut.defaultOptions);
    }
]);

// Routers
angular.module('micc').config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/panels', {
            template: '<p-panels></p-panels>'
        }).
        when('/panels/panel1', {
            template: '<p-panel-1></p-panel-1>'
        }).
        when('/panels/panel2', {
            template: '<p-panel-2></p-panel-2>'
        }).
        when('/panels/panel3', {
            template: '<p-panel-3></p-panel-3>'
        }).
        when('/panels/panel4', {
            template: '<p-panel-4></p-panel-4>'
        }).
        when('/panels/panel5', {
            template: '<p-panel-5></p-panel-5>'
        }).
        when('/panels/panel6', {
            template: '<p-panel-6></p-panel-6>'
        }).
        when('/panels/panel7', {
            template: '<p-panel-7></p-panel-7>'
        }).
        when('/panels/panel8', {
            template: '<p-panel-8></p-panel-8>'
        }).
        when('/panels/update', {
            template: '<p-update></p-update>'
        }).
        otherwise('/panels/panel1');
    }
]);

// Controller
angular.module('micc').controller('miccCtrl', ['$document', '$window', '$location', '$scope', '$interval', 'modelHTTP', 'coreConfig', 'coreHelperUpdate',
    function($document, $window, $location, $scope, $interval, modelHTTP, coreConfig, coreHelperUpdate) {
        var $ctrl = this;
        $ctrl.interval = [];
        $ctrl.$onInit = function () {
            // Check update status from cookie
            coreHelperUpdate.checkLocal();

            // Run auto update
            $ctrl.interval[0] = $interval(function(){
                if ($window.localStorage.getItem('update') !== 'running') {
                    modelHTTP.checkUpdate().then(coreHelperUpdate.setLocal);
                }
            }, coreConfig.app.updateInterval);

            // Keyboard listen
            $document.on('keyup', function(evt) {
                var path = $location.path();
                var panel = parseInt(path.substr(path.length - 1));
                //  Left
                if (evt.keyCode === 37) {
                    panel = panel === 1 ? 2 : panel;
                    $location.path('/panels/panel' + (panel - 1));
                }
                //  Right
                if (evt.keyCode === 39) {
                    panel = panel === 8 ? 7 : panel;
                    $location.path('/panels/panel' + (panel + 1));
                }
                //  Up
                if (evt.keyCode === 38) {
                    $location.path('/');
                }
                $scope.$apply();
            });

            // Listen location change
            $scope.$on("$locationChangeStart", function(event, newUrl) {
                // Block all page when update is running
                if ($window.localStorage.getItem('update') === 'running' && newUrl !== coreConfig.app.baseUrl + '#!/panels/update') {
                    event.preventDefault();
                    $location.path('/panels/update');
                }
            });
        };
    }
]);
