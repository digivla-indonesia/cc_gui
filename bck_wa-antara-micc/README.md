Use node 8.9.4
make sure the nodemailer version is 2.7.2 
and make sure the email-templates version is 2.6.0

if you are running a new os (in case centos 7.x), please follow this command:

  1. "curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash"
  2. Relogin SSH
  3. "yum install gcc-c++ compat-gcc-32 compat-gcc-32-c++"
  4. "nvm use 8.9.4"
  5. "npm install"
  6. "npm install -g pm2"
  7. go to the root project folder
  8. clonning file in ./cc_gui/wa-antara-micc/config/env/kemlu.js
  9. please change some part on that file
  10. running the apps with this command "CLIENT_NAME=CLIENT_NAME PORT=n pm2 start bin/www --name=cc_CLIENT_NAME". which n stands for port number, and CLIENT_NAME stands for the client id (based on kabayan's client name)