'use strict';

/**
 * Module dependencies.
 */
var request = require('request');

module.exports = function (options, callback) {
    var req = {
        //uri: global.omAPI + options.client + '/API',
        uri: global.omAPI,
        method: 'get'
    };

    request(req, function(err, httpResponse, body){
        if(err){
            return callback({
                success: false,
                data: err
            });
        }
        //console.log(JSON.parse(body));
        return callback({
            success: true,
            data: JSON.parse(body)
        });
    });
};
