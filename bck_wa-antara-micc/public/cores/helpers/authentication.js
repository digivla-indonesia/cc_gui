'use strict';
angular.module('core.helper.authentication', []);
angular.module('core.helper.authentication').service('Authentication', ['$http',
    function($http) {
        var service = this;
        service.getClientAssets = function (success) {
            $http.get('api/users/me').then(function(result){
                success(result.data);
            });
        };

        return service;
    }
]);
