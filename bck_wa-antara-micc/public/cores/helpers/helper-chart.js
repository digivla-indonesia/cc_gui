'use strict';
angular.module('core.helper.chart', []);
angular.module('core.helper.chart').service('coreHelperChart', ['coreConfig', 'coreHelperColor', '$window',
    function(coreConfig, coreHelperColor, $window) {
        var service = {};
        // Datasets creator for chart
        service.createDatasets = function (data, datasetsCustomOptions, mergeDatasets) {
            var length = data.length;
            var datasets = [];
            var colors = [];
            // Set up colors
            if(datasetsCustomOptions && datasetsCustomOptions.colors) {
                colors = datasetsCustomOptions.colors;
            }
            else if(length > 6) {
                colors = coreHelperColor.createColors(length);
            }
            else {
                colors = ['#2abc6e', '#63d6eb', '#ff0090', '#fa9315', '#fefefe', '#b6fb26'];
            }
            // Create datasets for each data
            var datasetsDefaultOptions = coreConfig.chart.datasetsDefaultOptions;
            for(var i = 0; i < data.length; i++) {
                // Add color
                datasetsDefaultOptions.borderColor = colors[i];
                datasetsDefaultOptions.backgroundColor = colors[i];
                datasetsDefaultOptions.pointHoverBorderColor = colors[i];
                datasetsDefaultOptions.pointHoverBackgroundColor = colors[i];
                // Override default option if specified
                if(datasetsCustomOptions && mergeDatasets) {
                    datasets.push(Object.assign({}, datasetsDefaultOptions, datasetsCustomOptions));
                }
                else if(datasetsCustomOptions && !mergeDatasets) {
                    datasets.push(Object.assign({}, datasetsCustomOptions));
                }
                else {
                    datasets.push(Object.assign({}, datasetsDefaultOptions));
                }
            }
            return datasets;
        };
        // Datasets creator for doughnut
        service.createDoughnutDatasets = function (data, datasetsCustomOptions, mergeDatasets) {
            var length = data.length;
            var datasets = {
                borderWidth: []
            };
            // Create datasets for each data
            for(var i = 0; i < data.length; i++) {
                // Hide border
                datasets.borderWidth.push(coreConfig.doughnut.datasetsDefaultOptions.borderWidth);
            }
            return datasets;
        };
        // Calculate weight
        service.calcWordWeight = function (words, size, canvas, width) {
            var percent = size / words.max * 100;
            var group = Math.floor(percent / 10) * 10;
            var weight = Math.pow(group, 3) / 10000;
            var canvasWidth = width ? width : canvas.offsetHeight;
            weight = weight < 18 ? 18 : weight;
            weight = weight * (canvasWidth / 650);
            return weight;
        };
        // Calculate vw
        service.calcVw = function (pixel) {
            var vpWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            return (pixel / coreConfig.screen.width) * vpWidth;
        };
        // Calculate vh
        service.calcVh = function (pixel) {
            var vpHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
            return (pixel / coreConfig.screen.height) * vpHeight;
        };
        // Fix chart font size
        service.fixFontSize = function (options) {
            options.scales.xAxes[0].scaleLabel.fontSize = service.calcVh(coreConfig.chart.defaultOptions.scales.xAxes[0].scaleLabel.fontSize);
            options.scales.xAxes[0].ticks.fontSize = service.calcVh(coreConfig.chart.defaultOptions.scales.xAxes[0].ticks.fontSize);
            options.scales.yAxes[0].scaleLabel.fontSize = service.calcVh(coreConfig.chart.defaultOptions.scales.yAxes[0].scaleLabel.fontSize);
            options.scales.yAxes[0].ticks.fontSize = service.calcVh(coreConfig.chart.defaultOptions.scales.yAxes[0].ticks.fontSize);
            options.legend.labels.fontSize = service.calcVh(coreConfig.chart.defaultOptions.legend.labels.fontSize);
            options.tooltips.titleFontSize = service.calcVh(coreConfig.chart.defaultOptions.tooltips.titleFontSize);
            return options;
        };
        return service;
    }
]);
