'use strict';
angular.module('model.alertSocket', ['ngResource']);
angular.module('model.alertSocket').factory('modelSocketAlertWatcher', ['$resource', '$http', '$interval', 'coreConfig',
    function($resource, $http, $interval, coreConfig) {
        var self = this;

        // self.getPanelData = function (param, success) {
        //     var httpRes = $http.get(coreConfig.app.onlinemonitoringAPI).then(getSuccess);

        //     function getSuccess(response) {
        //         var data = response.data.data['data_page'+param[0]];
        //         switch (param[1]) {
        //             case 'chart':
        //                 success(data.graph);
        //                 break;
        //             case 'progress':
        //                 var mapData = [];
        //                 for(var name in data.breakdown) {
        //                     mapData.push({name: name, percent: data.breakdown[name]});
        //                 }
        //                 success(mapData);
        //                 break;
        //             default:
        //                 success(data);
        //         }
        //     }

        //     return httpRes;
        // };

        var socket = io.connect(coreConfig.app.alertWatcher);

        self.on = function(eventName, callback){
            socket.on(eventName, callback);
        };

        self.emit = function(eventName, data) {
            socket.emit(eventName, data);
        };

        return self;
    }
]);
