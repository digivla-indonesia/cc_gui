'use strict';
angular.module('view.component.soundalert', []);
angular.module('view.component.soundalert').component('cSoundalert', {
    templateUrl: 'views/components/soundalert/soundalert.html',
    bindings: {
        play: '<',
        volume: '<'
    },
    controller: ['$element',
        function($element) {
            var $ctrl = this;
            var audio = $element.find('audio')[0];
            $ctrl.$onInit = function () {
                audio.autoplay = false;
                audio.controls = false;
                audio.loop = false;
                audio.volume = typeof($ctrl.volume) !== 'undefined' ? parseInt($ctrl.volume) / 100 : 0.25
            };
            $ctrl.$onChanges = function (changesObject) {
                //console.log(changesObject);
                $ctrl.toggle();
            };
            $ctrl.toggle = function () {
                if ($ctrl.play) {
                    audio.currentTime = 0;
                    audio.play();
                }
                else {
                    audio.pause();
                    audio.currentTime = 0;
                }
            };
        }
    ]
});
