'use strict';
angular.module('view.component.form.mediatype', []);
angular.module('view.component.form.mediatype').component('cFormMediatype', {
    templateUrl: 'views/components/forms/mediatype/mediatype.html',
    controller: [ 'coreConfig',
        function (coreConfig) {
            var $ctrl = this;
            $ctrl.prop = {
                radios: [
                    { index: '01', text: 'Print', value: 'print' },
                    { index: '02', text: 'Online Portal', value: 'online' },
                    { index: '03', text: 'All Mainstream', value: 'allmainstream' },
                    { index: '04', text: 'All Media', value: 'all' }
                ]
            };

            $ctrl.model = coreConfig.app.defaultMediaType;
        }
    ],
    bindings: {
        model:'='
    }
});
