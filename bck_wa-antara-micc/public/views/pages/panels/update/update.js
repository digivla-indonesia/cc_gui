'use strict';
angular.module('view.page.panels.update', []);
angular.module('view.page.panels.update').component('pUpdate', {
    templateUrl: 'views/pages/panels/update/update.html',
    controller: ['$interval', '$location', '$window', 'modelHTTP',
        function($interval, $location, $window, modelHTTP) {
            var $ctrl = this;
            $ctrl.interval = [];
            $ctrl.$onInit = function () {
                if ($window.localStorage.getItem('update') === 'running') {
                    $ctrl.interval[0] = $interval(function(){
                        modelHTTP.checkUpdate().then(onCheckRunningUpdate);
                    }, 5000);
                }
                else {
                    window.location.href = document.referrer;
                }
            };
            function onCheckRunningUpdate(response) {
                if(response.data.status === 'finish') {
                    $window.localStorage.setItem('update', 'finish');
                    window.location.href = document.referrer;
                }
            }
        }
    ]
});
