'use strict';
angular.module('view.page.panels.panel-2', []);
angular.module('view.page.panels.panel-2').component('pPanel2', {
    templateUrl: 'views/pages/panels/panel-2/panel-2.html',
    controller: ['$interval', '$window', '$scope', '$element', 'coreConfig', 'coreHelper', 'coreHelperColor', 'coreHelperChart', 'modelMention',
        function($interval, $window, $scope, $element, coreConfig, coreHelper, coreHelperColor, coreHelperChart, modelMention) {
            var $ctrl = this;
            $ctrl.interval = [];
            $ctrl.timeout = [];
            $ctrl.model = {
                mention: {}
            };
            $ctrl.$onInit = function () {
                // Get mention data
                $ctrl.model.mention.chart = {};
                $ctrl.model.mention.progress = {};
                $ctrl.model.mention.doughnut = {};
                $ctrl.model.mention.doughnut2 = {};

                modelMention.getPanelData([2, 'chart'], onMentionChart);
                modelMention.getPanelData([2, 'progress_2'], onMentionProgress);
                modelMention.getPanelData([2, 'doughnut'], onMentionDoughnut);

                angular.element($window).bind('resize', onWindowResize);
            };
            $ctrl.$onDestroy = function () {
                $interval.cancel($ctrl.interval[0]);
                $interval.cancel($ctrl.interval[1]);
                $interval.cancel($ctrl.interval[2]);
                $interval.cancel($ctrl.timeout[0]);
                $interval.cancel($ctrl.timeout[1]);
                $interval.cancel($ctrl.timeout[2]);
                $interval.cancel($ctrl.timeout[3]);
            };
            $ctrl.chartUpdate = function () {
                $ctrl.interval[0] = $interval(function () {
                    modelMention.getPanelData([2, 'chart'], function(chart) {
                        $ctrl.model.mention.chart = Object.assign({}, $ctrl.model.mention.chart, chart);
                    });
                }, coreConfig.app.refreshInterval);
            };
            $ctrl.progressUpdate = function () {
                $ctrl.interval[1] = $interval(function () {
                    modelMention.getPanelData([2, 'progress_2'], function (progress) {
                        $ctrl.model.mention.progress = Object.assign({}, $ctrl.model.mention.progress, progress);

                        $ctrl.timeout[0] = $interval(function () {
                            $ctrl.storeImage2($element);
                            $interval.cancel($ctrl.timeout[0]);
                        }, 2000);
                    });
                }, coreConfig.app.refreshInterval);
            };
            $ctrl.doughnutUpdate = function () {
                $ctrl.interval[2] = $interval(function () {
                    modelMention.getPanelData([2, 'doughnut'], function (doughnut) {
                        $ctrl.model.mention.doughnut = Object.assign({}, $ctrl.model.mention.doughnut, doughnut);

                        $ctrl.timeout[1] = $interval(function () {
                            $ctrl.storeImage($element);
                            $interval.cancel($ctrl.timeout[1]);
                        }, 2000);
                    });
                }, coreConfig.app.refreshInterval);
            };
            $ctrl.storeImage = function ($element) {
                coreHelper.initReport();
                var canvas = $element.find("#js-doughnut-positive")[0];
                var localReport = JSON.parse($window.localStorage.getItem('report'));
                localReport.images.doughnut_1 = { base64: canvas.toDataURL() };
                $window.localStorage.setItem('report', JSON.stringify(localReport));
            };
            $ctrl.storeImage2 = function ($element) {
                coreHelper.initReport();
                var canvas = $element.find("#js-doughnut-positive-breakdown-hidden")[0];
                var localReport = JSON.parse($window.localStorage.getItem('report'));
                localReport.images.doughnut_2 = { base64: canvas.toDataURL() };
                $window.localStorage.setItem('report', JSON.stringify(localReport));
            };

            function onMentionChart(chart) {
                $ctrl.model.mention.chart = chart;
                // Add global configuration
                $ctrl.model.mention.chart.options = angular.copy(coreConfig.chart.defaultOptions);
                // Add configurations for each line
                $ctrl.model.mention.chart.datasets = coreHelperChart.createDatasets(chart.data);
                // console.log($ctrl.model.mention);
                // Fix chart font size
                $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);

                $ctrl.chartUpdate();
            }
            function onMentionProgress(progress) {
                $ctrl.model.mention.progress = progress;
                //console.log(progress);

                // Add color configurations for each doughnut
                $ctrl.model.mention.progress.doughnut.colors = coreHelperColor.randomizeColors(['#fa941f', '#f72770', '#a3df2c', '#64d6ec', '#a280fc']);
                // Add default configurations for each doughnut
                $ctrl.model.mention.progress.doughnut.datasets = coreHelperChart.createDoughnutDatasets(progress.doughnut.data);

                //console.log($ctrl.model.mention.progress.doughnut);
                $ctrl.timeout[2] = $interval(function () {
                    $ctrl.storeImage2($element);
                    $interval.cancel($ctrl.timeout[2]);
                }, 2000);
                $ctrl.progressUpdate();
            }
            function onMentionDoughnut(doughnut) {
                $ctrl.model.mention.doughnut = doughnut;
                //console.log(doughnut);

                // Add color configurations for each doughnut
                $ctrl.model.mention.doughnut[0].colors = [coreConfig.colors.positive, coreConfig.colors.others];
                $ctrl.model.mention.doughnut[1].colors = [coreConfig.colors.netral, coreConfig.colors.others];
                $ctrl.model.mention.doughnut[2].colors = [coreConfig.colors.negative, coreConfig.colors.others];
                // Add default configurations for each doughnut
                $ctrl.model.mention.doughnut[0].datasets = coreHelperChart.createDoughnutDatasets(doughnut[0].data);
                $ctrl.model.mention.doughnut[1].datasets = coreHelperChart.createDoughnutDatasets(doughnut[1].data);
                $ctrl.model.mention.doughnut[2].datasets = coreHelperChart.createDoughnutDatasets(doughnut[2].data);
                // console.log($ctrl.model.mention);

                $ctrl.timeout[3] = $interval(function () {
                    $ctrl.storeImage($element);
                    $interval.cancel($ctrl.timeout[3]);
                }, 2000);
                $ctrl.doughnutUpdate();
            }
            function onWindowResize() {
                // Fix chart font size
                if ($ctrl.model.mention.chart.options) {
                    $ctrl.model.mention.chart.options = coreHelperChart.fixFontSize($ctrl.model.mention.chart.options);
                    $scope.$apply();
                }
            }
        }
    ]
});
