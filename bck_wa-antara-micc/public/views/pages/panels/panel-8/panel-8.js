'use strict';
angular.module('view.page.panels.panel-8', []);
angular.module('view.page.panels.panel-8').component('pPanel8', {
    templateUrl: 'views/pages/panels/panel-8/panel-8.html',
    controller: ['Authentication', '$scope', '$document', '$element', '$timeout', '$interval', 'modelMention', 'modelTest', 'modelHTTP', 'coreConfig', '$ync',
        function(Authentication, $scope, $document, $element, $timeout,  $interval, modelMention, modelTest, modelHTTP, coreConfig, $ync) {
            var $ctrl = this;
            $ctrl.periodSelection = {};
            $ctrl.model = {
                post: {
                    posts: []
                }
            };
            $ctrl.prop = {
                mediaplayer: {
                    post: {}
                }
            };


            // list controller variables
            $ctrl.resultsToDisplay = [];

            $ctrl.$onInit = function () {
                var categoryDatas = {
                    "url": "/client/getassets",
                    "method": "POST",
                    "data": {}
                };

                modelHTTP.HTTPPost('/api/digivla-requester', categoryDatas).success(function(response){
                    $ctrl.categorySet = response.data.data.data;
                    $ctrl.categorySelection = $ctrl.categorySet[0].category_set;

                    $scope.categorySelection = $ctrl.categorySelection;
                    $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                    $scope.periodSelection = $ctrl.periodSelection;

                    changeFilter();


                    // refresh and variable's watcher
                    $timeout(function(){
                        $scope.$watch(function(){
                            return $ctrl.categorySelection;
                        }, function(){
                            $scope.categorySelection = $ctrl.categorySelection;
                            return changeFilter();
                        });

                        $scope.$watch(function(){
                            return $ctrl.mediaTypeSelection;
                        }, function(){
                            $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                            return changeFilter();
                        });

                        $scope.$watch(function(){
                            return $ctrl.periodSelection;
                        }, function(){
                            $scope.periodSelection = $ctrl.periodSelection;
                            return changeFilter();
                        });


                        // between browsers
                        $scope.$watch('categorySelection', function(newValues){
                            if ($ctrl.categorySelection !== newValues) {
                                $ctrl.categorySelection = newValues;
                                $scope.categorySelection = $ctrl.categorySelection;
                                return changeFilter();
                            }
                        });

                        $scope.$watch('mediaTypeSelection', function(newValues){
                            if ($ctrl.mediaTypeSelection !== newValues) {
                                $ctrl.mediaTypeSelection = newValues;
                                $scope.mediaTypeSelection = $ctrl.mediaTypeSelection;
                                return changeFilter();
                            }
                        });

                        $scope.$watch('periodSelection', function(newValues){
                            if ($ctrl.periodSelection !== newValues) {
                                $ctrl.periodSelection = newValues;
                                $scope.periodSelection = $ctrl.periodSelection;
                                return changeFilter();
                            }
                        });

                        // auto refresh
                        $interval(function(){
                            pushToList();
                        }, coreConfig.app.refreshInterval);
                        
                    }, 5000);


                });
            };
            $ctrl.setMediaplayer = function (post) {
                $ctrl.prop.mediaplayer.post = {
                    type: post.type,
                    file_pdf: post.src
                };
            };

            function changeFilter(){
                var datas = {
                    "url": "/formed/getnewslist",
                    "method": "POST",
                    "data": {
                        "gc": $ctrl.categorySelection,
                        "sc": "All Category",
                        "tf": $ctrl.periodSelection.tf,
                        "df": $ctrl.periodSelection.df,
                        "dt": $ctrl.periodSelection.dt,
                        "media_type": $ctrl.mediaTypeSelection
                    }
                };

                modelHTTP.HTTPPost('/api/digivla-requester', datas).success(function(response){
                    $ctrl.model.post.posts = [];
                    $ctrl.resultsBucket = [];
                    $ctrl.resultsBucket = response.data.data.data;
                    $ctrl.offset = 0;

                    var file = response.data.data.tv.file_pdf;
                    var tanggal = file.substring(8, 10);
                    var bulan = file.substring(5, 7);
                    var tahun = file.substring(0, 4);
                    var file_url = 'http://pdf.antara-insight.id/media_tv/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;
                    $ctrl.setMediaplayer({src: file_url, type: 'video'});
                    pushToList();
                });
            }

            function pushToList(resultsToDisplay, offset, resultsBucket){
                var tmpArr = [];
                var file, tanggal, bulan, tahun, file_url;

                if( ($ctrl.offset + coreConfig.app.limitsToDisplay) > $ctrl.resultsBucket.length ){
                    for(var i=$ctrl.offset;i<$ctrl.resultsBucket;i++){
                        file = $ctrl.resultsBucket[i].detail.file_pdf;
                        tanggal = file.substring(8, 10);
                        bulan = file.substring(5, 7);
                        tahun = file.substring(0, 4);
                        file_url = 'http://pdf.antara-insight.id/pdf_images/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;
                        $ctrl.resultsBucket[i].detail.file_pdf = file_url;

                        $ctrl.model.post.posts.push($ctrl.resultsBucket[i].detail);
                    }
                    $ctrl.offset = 0;
                } else{
                    for(var i=$ctrl.offset;i<coreConfig.app.limitsToDisplay;i++){
                        file = $ctrl.resultsBucket[i].detail.file_pdf;
                        tanggal = file.substring(8, 10);
                        bulan = file.substring(5, 7);
                        tahun = file.substring(0, 4);
                        file_url = 'http://pdf.antara-insight.id/pdf_images/' + tahun + '/' + bulan + '/' + tanggal + '/' + file;
                        $ctrl.resultsBucket[i].detail.file_pdf = file_url;

                        $ctrl.model.post.posts.push($ctrl.resultsBucket[i].detail);
                    }

                    $ctrl.offset += coreConfig.app.limitsToDisplay;
                }

            }

            Authentication.getClientAssets(function(result){
                $YNC.latencyTest = 100
                $ync($scope, ['categorySelection', 'periodSelection', 'mediaTypeSelection'], result.clientSocket);
            });

        }
    ]
});
