/**
 * Created by olyjosh on 29/06/2017.
 */

var sender = 'smtps://info@antara-insight.id';   // The emailto use in sending the email(Change the @ symbol to %40 or do a url encoding )
var password = 'digivla@cikatomas123';  // password of the email to use
var from = 'info@antara-insight.id';  // Sender email address
var fileServerPath = 'http://159.89.196.121/cc/report_attachment/';  // Sender email address

var path = require('path');
var clientConfig = require(path.join(process.cwd(), 'config/env/', process.env.CLIENT_NAME)) || {};
var nodeMailer = require("nodemailer");
var EmailTemplate = require('email-templates').EmailTemplate;
var fs = require("fs");
var sshClient = require('ssh2');
var sharp = require('sharp');


var transporter = nodeMailer.createTransport(sender + ':' + password + '@smtp.zoho.com');


exports.sendPasswordReset = function (email, username, name, tokenUrl) {
    // create template based sender function
    // assumes text.{ext} and html.{ext} in template/directory
    var sendResetPasswordLink = transporter.templateSender(
        // new EmailTemplate('./templates/resetPassword'), {
        new EmailTemplate('./templates/formedAlert'), {
            from: from
    });

    // transporter.template
    sendResetPasswordLink({
        to: email,
        subject: 'Password Reset - YourDomain.com'
    }, {
        name: name,
        username: username,
        token: tokenUrl
    }, function (err, info) {
        if (err) {
            console.log(err)
        } else {
            console.log('Link sent\n' + JSON.stringify(info));
        }
    });
};


exports.sendAlert = function (options) {
    // create template based sender function
    // assumes text.{ext} and html.{ext} in template/directory
    var sendAlertLink = transporter.templateSender(
        // new EmailTemplate('./templates/resetPassword'), {
        new EmailTemplate('./templates/formedAlert'), {
            from: from
    });

    // transporter.template
    sendAlertLink({
        // to: options.recipients,
        to: clientConfig.clientEmail.join(', '),
        subject: 'Formal Media Alert - antara-insight.id'
    }, {
        categories: options.datas,
        client_id: options.client_id
    }, function (err, info) {
        if (err) {
            console.log(err)
        } else {
            console.log('Link sent\n' + JSON.stringify(info));
        }
    });
};

exports.sendReport = function (options) {
    var dateNow = Date.now();
    var dateNowObj = new Date(dateNow);
    //console.log(options);
    // Add additional data
    options.date = dateNowObj.toString();
    options.path = fileServerPath;
    options.logo = fileServerPath + 'assets/logo_antara_insight_color_210x61px.png';
    // Save images
    for (var name in options.data.images) {
        console.log(name);
        options.data.images[name].path = __dirname + "/templates/report/overview/images/"+name+".png";
        options.data.images[name].pathTmp = __dirname + "/templates/report/overview/images/"+name+".png.tmp";
        options.data.images[name].url = fileServerPath + options.data.client + "/" + name + "-" + dateNow + ".png";

        if (name === 'doughnut_1') {
            sharp(Buffer.from(options.data.images[name].base64.replace(/^data:image\/png;base64,/, ""), 'base64'))
                .resize(150, 150, {kernel: sharp.kernel.nearest})
                .crop('center')
                .png({compressionLevel: 0, force: true})
                .toFile(options.data.images[name].path, function(err, info) {
                    if (err) console.log(err);
                    else {
                        console.log('[FS] ' + name + '.png is saved');
                    }
                });
        }
        if (name === 'doughnut_2') {
            sharp(Buffer.from(options.data.images[name].base64.replace(/^data:image\/png;base64,/, ""), 'base64'))
                .resize(320, 320, {kernel: sharp.kernel.nearest})
                .crop('center')
                .png({compressionLevel: 0, force: true})
                .toFile(options.data.images[name].path, function(err, info) {
                    if (err) console.log(err);
                    else {
                        console.log('[FS] ' + name + '.png is saved');
                    }
                });
        }
        if (name === 'wordcloud') {
            fs.writeFile(
                options.data.images[name].path,
                options.data.images[name].base64.replace(/^data:image\/png;base64,/, ""),
                'base64',
                function(err) {
                    if (err) console.log(err);
                    else {
                        console.log('[FS] ' + name + '.png is saved');
                    }
                }
            );
        }

        /*fs.writeFile(
            options.data.images[name].pathTmp,
            options.data.images[name].base64.replace(/^data:image\/png;base64,/, ""),
            'base64',
            function(err) {
                if (err) console.log(err);
                else {

                }
            }
        );*/
    }

    // Upload images
    var ssh = new sshClient();
    ssh.on('ready', function() {
        console.log('[SSH] READY');
        ssh.exec('pwd', function(err, shell) {
            shell.on('data', function(data) {
                console.log('[SSH] ' + data);
            });
        });
        ssh.sftp(function(err, sftp) {
            sftp.readdir("/sites/apps/file_server/cc/report_attachment", function(err, list) {
                var dirExist = false;
                for(var i in list) {
                    if (list[i].filename === options.data.client) {
                        dirExist = true;
                    }
                }
                if (!dirExist) {
                    sftp.mkdir("/sites/apps/file_server/cc/report_attachment/" + options.data.client, function (err) {
                        console.log('[SFTP] Creating directory "' + options.data.client + '"');
                        if (err) {
                            console.log('[SFTP]' + err);
                            ssh.end();
                            return false;
                        }
                    });
                }
                for (var name in options.data.images) {
                    var localFile = options.data.images[name].path;
                    var remoteDir = "/sites/apps/file_server/cc/report_attachment/" + options.data.client + "/";
                    var remoteFile = name + "-" + dateNow + ".png";
                    var remotePath = remoteDir + remoteFile;
                    console.log('[SFTP] Uploading "' + localFile + '" to "' + remotePath + '"');
                    sftp.fastPut(localFile, remotePath, {}, function(err) {
                        if (err) {
                            console.log('[SFTP]' + err);
                        }
                        else {
                            console.log('[SFTP] "' + localFile + '" uploaded');
                        }
                    });
                }
            });
        });
    });
    ssh.on('error', function(err) {
        console.log(err);
    });
    ssh.on('end', function() {
        console.log('[SSH] SSH is ended.');
    });
    ssh.connect({
        host: '159.89.196.121',
        port: 22,
        user: 'cc',
        password: 'SPX,>E{+^6g<jkwn'
    });

    // Render and send ejs template
    var reportTemplate = new EmailTemplate('./templates/report/overview');
    var reportSender = transporter.templateSender(reportTemplate, { from: from });
    reportSender({
        // to: options.to,
        to: clientConfig.clientEmail.join(', '),
        subject: options.subject
    }, options, function(error, info){
        if(error){
            return console.log('[SMTP]' + error);
        }
        return console.log('[SMTP]' + info);
    });
    reportTemplate.render(options, function (err, result) {
        if (err) { return console.error(err); }
        fs.writeFile('./templates/report/overview/render/html.html', result.html, function(err) {
            if(err) { return console.log(err); }
            return console.log("[FS] html.html is saved.");
        });
    });
};
